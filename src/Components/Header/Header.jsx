import React from "react";
import { NavLink, Link } from "react-router-dom";
import { Container, Navbar, Nav } from "react-bootstrap";
import { FaAngleRight } from "react-icons/fa";
import styled from "styled-components";
import appLogo from "../../assets/images/lmex-logo.svg";

const HeaderContent = styled.div`
  .mainNavigation {
    padding: 14px 40px;
    @media (max-width: 767px) {
      padding: 14px 16px;
    }
  }
  .mainNav {
    @media (max-width: 767px) {
      margin-top: 15px;
    }
    .nav-link {
      color: #ffffff;
      padding: 18px 39px;
      font-family: "Poppins";
      font-style: normal;
      font-weight: normal;
      font-size: 16px;
      line-height: 24px;
      position: relative;
      &:hover,
      &:focus {
        color: #ffffff;
      }
    }
    .nav-link.active {
      color: #006de4 !important;
    }
    .nav-link.active::before {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      width: 100%;
      height: 5px;
      background: #006de4;
      margin-top: -14px;
      @media (max-width: 767px) {
        width: 5px;
        height: 100%;
        margin-top: 0;
      }
    }
  }
`;
const Header = () => {
  return (
    <HeaderContent>
      <Navbar collapseOnSelect expand="lg" className="mainNavigation">
        <Container>
          <Navbar.Brand as={Link} to="/" className="py-0">
            <img src={appLogo} />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ml-auto mainNav">
              <Nav.Link as={NavLink} to="/market" activeClassName="active">
                Market
              </Nav.Link>
              <Nav.Link as={NavLink} to="/products" activeClassName="active">
                Products
              </Nav.Link>
              <Nav.Link as={NavLink} to="/about-us" activeClassName="active">
                About
              </Nav.Link>
              <Nav.Link
                as={NavLink}
                to="/help-and-support"
                activeClassName="active"
              >
                Support
              </Nav.Link>
              <Nav.Link as={NavLink} to="/signup" activeClassName="active">
                Login
              </Nav.Link>
              <Nav.Link as={Link} to="/" className="btn btn-primary blueBtn">
                Start Trading <FaAngleRight className="ml-3" />
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </HeaderContent>
  );
};

export default Header;
