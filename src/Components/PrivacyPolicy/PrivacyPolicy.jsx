import React from "react";

import { Container, Row, Col } from "react-bootstrap";
import styled from "styled-components";
import PageFooter from "../PageFooter";
import PageSubFooter from "../PageSubFooter";
import PageHeading from "../SubComponents/PageHeading";
import PageTitle from "../SubComponents/PageHeading";

const PrivacyPolicyContainer = styled.div``;
const PrivcyPolicyContent = styled.div`
  margin-bottom: 120px;
  p {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 22px;
    color: #ffffff;
    text-align: left;
    margin-bottom: 0;
  }
  .mb30 {
    margin-bottom: 30px !important;
  }
  .mb55 {
    margin-bottom: 55px !important;
  }
  .titleHeading {
    font-size: 20px;
    line-height: 28px;
  }
`;
const PrivacyPolicy = () => {
  return (
    <PrivacyPolicyContainer>
      <Container>
        <PageHeading
          pageTitle="LMEX Privacy Policy"
          pageSubTitle="Last updated Oct 12, 2021"
        />

        <Row>
          <Col>
            <PrivcyPolicyContent>
              <div className="mb30">
                <p>
                  This Privacy Policy and any terms expressly incorporated
                  herein (“Terms”) applies to any person (natural person or
                  otherwise) (“User/ You”) accessing or using, any services made
                  available by Bit Protocol LLC (“Bit Protocol LLC”), and its
                  affiliates (“the Group/Us/Our/We”) on this website (“Website”)
                  or on Our mobile application, and to any other related product
                  or services provided by Us (collectively, the “Services”). Our
                  Services are offered through one or more affiliates of Bit
                  Protocol LLC. By using the Services, you understand that your
                  data may be used by any one of the following entities:
                </p>
              </div>
              <div className="mb30">
                <p>
                  <strong>Bit Protocol LLC</strong> a company incorporated in St
                  Vincent and the Grenadines
                </p>
                <p>
                  <strong>Dote Labs Ltd</strong> a company incorporated in the
                  British Virgin Islands
                </p>
                <p>
                  By accessing or using any Services in any manner whatsoever,
                  You agree to be bound by these Terms.
                </p>
              </div>
              <div className="mb30">
                <p>
                  The Privacy Policy and any other policies communicated by Us
                  shall be applicable to Your use of the Services and shall be
                  deemed incorporated herein by reference.
                </p>
              </div>
              <div className="mb55">
                <p>
                  The services we offer include but are not limited to a
                  peer-to-peer trading platform that offers trading of
                  cryptographic tokens and currencies (“Cryptocurrency“ or
                  “Cryptocurrencies”) and leveraged derivative contracts that
                  have Cryptocurrencies as the underlying basis for the
                  transaction (“Cryptocurrency Derivative” or “Cryptocurrency
                  Derivatives” or “Derivative Contract” or “Derivative
                  Contracts”).
                </p>
              </div>
              <div className="mb30">
                <p>
                  Information we collect automatically when you use the Services
                  or the Website When You access or use our Website or Services,
                  we automatically collect information about You, including:
                </p>
              </div>
              <div className="mb30">
                <p>Device Information:</p>
                <p>
                  We collect information about the computer or mobile device You
                  use to access our Services, including the hardware model,
                  operating system and version, unique device identifiers, and
                  mobile network information.
                </p>
              </div>
              <div className="mb30">
                <p>Exchange information:</p>
                <p>
                  We collect information about the transactions You complete
                  using the Services, including the Cryptocurrency or
                  Cryptocurrency Derivatives trades You execute, the deposits
                  and withdrawals you make in/ from your Hosted Wallets.
                </p>
              </div>

              <div className="mb30">
                <p className="titleHeading">
                  Information collected by cookies and other tracking
                  technologies
                </p>
                <p>
                  We and our service providers may use various technologies to
                  collect information, including cookies and web beacons.
                  Cookies are small data files stored on Your hard drive or in
                  device memory that help us improve our Services and Your
                  experience, see which areas and features of our Services are
                  popular and count visits, manage the registration process for
                  accounts, remember your site preferences, retain certain
                  information to process orders for exchange transactions, and
                  retain information to provide You with support. Web beacons
                  are electronic images that may be used in our Services or
                  emails and help deliver cookies, count visits, and understand
                  usage and campaign effectiveness.
                </p>
              </div>
              <div className="mb55">
                <p>Details of our cookies policy are available here.</p>
              </div>
              <div className="mb30">
                <p className="titleHeading">Use of information</p>
                <p>
                  We may use information about You for various purposes,
                  including to:
                </p>
              </div>
              <div className="mb55">
                <p>Provide, maintain, and improve Our Services;</p>
                <p>
                  Provide and deliver the products and services You request,
                  process transactions, and send You related information,
                  including confirmations and invoices;
                </p>
                <p>
                  Send You technical notices, updates, security alerts, and
                  support and administrative messages;
                </p>
                <p>
                  Respond to Your comments, questions and requests, and provide
                  customer service;
                </p>
                <p>Resolve disputes and troubleshoot problems;</p>
                <p>
                  Monitor and analyse trends, usage, and activities in
                  connection with Our Services;
                </p>
                <p>
                  Link or combine with information We get from others to help
                  understand Your needs and provide You with better service; and
                </p>
                <p>
                  Carry out any other purpose for which the information was
                  collected.
                </p>
                <p>
                  We also reserve the right to use aggregated personal data to
                  understand how our users use our Services, provided that those
                  data cannot identify any individual.
                </p>
              </div>

              <div className="mb30">
                <p className="titleHeading">Sharing and Disclosure</p>
                <p>
                  We may share your information with selected recipients to
                  perform functions required to provide certain Services to you
                  and/or in connection with our efforts to prevent and
                  investigate fraudulent or other criminal activity. All such
                  third parties will be contractually bound to protect data in
                  compliance with our Privacy Policy.
                </p>
              </div>
              <div className="mb30">
                <p>The categories of recipients include:</p>
              </div>

              <p>
                Companies within the Group in order to provide the Services to
                you.
              </p>
              <p>
                Cloud service providers to store certain personal data and for
                disaster recovery services, as well as, for the performance of
                any contract we enter into with you.
              </p>
              <p>
                anyone to whom we lawfully transfer or may transfer our rights
                and duties under the relevant terms and conditions governing the
                use of any of the Services;
              </p>
              <p>
                any third party as a result of any restructure, sale or
                acquisition of our group or any Affiliates, provided that any
                recipient uses your information for the same purposes as it was
                originally supplied to us and/or used by us; and
              </p>
              <p>
                companies and organisations that assist us in processing,
                verifying or refunding transactions/orders you make and in
                providing any of the Services that you have requested;
              </p>
              <p>
                external service providers who provide “know your customer”
                services and/ or Google analytics etc., to assist Us in
                collating and analysing User information and enabling us to make
                the use of the Services and the Website more user-friendly.
              </p>
              <p>
                identity verification agencies to undertake required
                verification checks;
              </p>
              <p>
                fraud or crime prevention agencies to help fight against crimes
                including fraud, money-laundering and terrorist financing;
              </p>
              <p>
                organisations which assist us with customer service facilities;
              </p>
              <div className="mb30">
                <p>
                  regulatory and law enforcement authorities, where the law
                  allows or requires us to do so. In the event Your personal
                  information is required to be shared with any authority in
                  compliance to the applicable existing laws of any appropriate
                  and applicable jurisdiction, You will provide Your prior
                  written consent to Us within 5 (Five) days, after which (in
                  case of no response from You), We will be permitted to
                  disclose such personal information of Yours with the said
                  authority.
                </p>
              </div>
              <p>
                Your payment and transaction details will not be shared with
                other users or third parties under any circumstances.
              </p>
            </PrivcyPolicyContent>
          </Col>
        </Row>
        <Row>
          <Col>
            <PageSubFooter />
            <PageFooter />
          </Col>
        </Row>
      </Container>
    </PrivacyPolicyContainer>
  );
};

export default PrivacyPolicy;
