import React from "react";

import { Link } from "react-router-dom";
import { Row, Col, Card } from "react-bootstrap";
import styled from "styled-components";
import { BsTriangleFill } from "react-icons/bs";
import { FaChevronRight } from "react-icons/fa";

const ForexCardContainer = styled.div``;

const ForexCardBox = styled(Card)`
  background: linear-gradient(
    180deg,
    rgba(34, 49, 71, 0.7) 0%,
    rgba(34, 49, 71, 0.07) 100%
  );
  border: 1px solid rgba(255, 255, 255, 0.1);
  box-sizing: border-box;
  backdrop-filter: blur(100px);
  border-radius: 8px;

  padding: 20px;
  min-height: 60px;
  height: 100%;

  .forexCardTitle {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
    color: #ffffff;
  }

  .forexTradeValue {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
  }

  .allCard {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    color: #006de4;
  }
`;

const ForexCard = () => {
  const forexCardInfo = [
    {
      stockType: "AUD/USD",
      stockState: BsTriangleFill,
      stockValue: "5.2",
    },
    {
      stockType: "AUD/CAD",
      stockState: BsTriangleFill,
      stockValue: "16.1",
    },
    {
      stockType: "EUR/GBP",
      stockState: BsTriangleFill,
      stockValue: "8.5",
    },
    {
      stockType: "EUR/CHF",
      stockState: BsTriangleFill,
      stockValue: "16.1",
    },
  ];
  return (
    <ForexCardContainer>
      <Row>
        {forexCardInfo.map((forex, index) => (
          <Col sm={3} key={index}>
            <div className="mb30">
              <ForexCardBox>
                <Card.Body className="p-0">
                  <Card.Text>
                    <div className="d-flex justify-content-between align-items-center">
                      <div className="forexCardTitle">{forex.stockType}</div>

                      <div className="d-flex align-items-center forexTradeValue greenText">
                        <span className="upTriangle">
                          <forex.stockState />
                        </span>
                        <span className="ml-1">{forex.stockValue}</span>
                      </div>
                    </div>
                  </Card.Text>
                </Card.Body>
              </ForexCardBox>
            </div>
          </Col>
        ))}
        <Col sm={3}>
          <div className="mb30">
            <Link to="/">
              <ForexCardBox className="d-flex justify-content-center align-items-center">
                <div className="d-flex justify-content-center align-items-center allCard">
                  <span>Explore All Forex</span>
                  <FaChevronRight className="ml-3" />
                </div>
              </ForexCardBox>
            </Link>
          </div>
        </Col>
      </Row>
    </ForexCardContainer>
  );
};

export default ForexCard;
