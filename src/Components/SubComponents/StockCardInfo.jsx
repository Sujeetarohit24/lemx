import React from "react";

import { Link } from "react-router-dom";
import { Row, Col, Card } from "react-bootstrap";
import styled from "styled-components";
import { BsTriangleFill } from "react-icons/bs";
import { FaChevronRight } from "react-icons/fa";

const StockCardContainer = styled.div``;

const StockCard = styled(Card)`
  background: linear-gradient(
    180deg,
    rgba(34, 49, 71, 0.7) 0%,
    rgba(34, 49, 71, 0.07) 100%
  );
  border: 1px solid rgba(255, 255, 255, 0.2);
  box-sizing: border-box;
  box-shadow: 0px 0px 15px rgba(255, 255, 255, 0.07);
  backdrop-filter: blur(100px);
  border-radius: 8px;
  padding: 20px;
  min-height: 130px;
  height: 100%;

  .stockCardTitle {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
    color: #ffffff;
  }
  .stockCardSubTitle {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 14px;
    color: #798595;
  }

  .stockTradeValue {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
  }

  .stockTradeMark {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 21px;
    text-align: right;
    color: #798595;
  }
  .allCard {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    color: #006de4;
  }
`;

const StockCardInfo = () => {
  const stockCardInfo = [
    {
      stockType: "DIVIS LABS",
      stockTypeShortCode: "NSE",
      stockState: BsTriangleFill,
      stockValue: "5220.70",
      stockRate: "+390.10 (+8.03%)",
      stockTradeHighValue: "HIGH: 1405.55",
      stockTradeLowValue: "LOW: 1372.05",
    },
    {
      stockType: "TECHM",
      stockTypeShortCode: "NSE",
      stockState: BsTriangleFill,
      stockValue: "1399.05",
      stockRate: "+27.80 (+2.03%)",
      stockTradeHighValue: "HIGH: 1405.55",
      stockTradeLowValue: "LOW: 1372.05",
    },
    {
      stockType: "TATA MOTORS",
      stockTypeShortCode: "NSE",
      stockState: BsTriangleFill,
      stockValue: "342.00",
      stockRate: "+8.70 (+2.61%)",
      stockTradeHighValue: "HIGH: 1405.55",
      stockTradeLowValue: "LOW: 1372.05",
    },
    {
      stockType: "BEL",
      stockTypeShortCode: "NSE",
      stockState: BsTriangleFill,
      stockValue: "205.80",
      stockRate: "+4.05 (+2.03%)",
      stockTradeHighValue: "HIGH: 1405.55",
      stockTradeLowValue: "LOW: 1372.05",
    },
  ];
  return (
    <StockCardContainer>
      <Row>
        {stockCardInfo.map((stock, index) => (
          <Col sm={3} key={index}>
            <div className="mb30">
              <StockCard>
                <Card.Body className="p-0">
                  <Card.Text>
                    <div className="d-flex justify-content-between align-items-center mb30">
                      <div>
                        <div className="stockCardTitle">{stock.stockType}</div>
                        <div className="stockCardSubTitle">
                          {stock.stockTypeShortCode}
                        </div>
                      </div>
                      <div>
                        <div className="d-flex align-items-center ml-auto stockTradeValue greenText">
                          <span className="upTriangle">
                            <stock.stockState />
                          </span>
                          <span className="ml-1">{stock.stockValue}</span>
                        </div>
                        <div className="stockCardSubTitle">
                          {stock.stockRate}
                        </div>
                      </div>
                    </div>
                  </Card.Text>
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="stockTradeMark">
                      {stock.stockTradeHighValue}
                    </div>
                    <div className="stockTradeMark">
                      {stock.stockTradeLowValue}
                    </div>
                  </div>
                </Card.Body>
              </StockCard>
            </div>
          </Col>
        ))}
        <Col sm={3}>
          <div className="mb30">
            <Link to="/">
              <StockCard className="d-flex justify-content-center align-items-center ">
                <div className="d-flex justify-content-center align-items-center allCard">
                  <span>Explore All Stocks</span>
                  <FaChevronRight className="ml-3" />
                </div>
              </StockCard>
            </Link>
          </div>
        </Col>
      </Row>
    </StockCardContainer>
  );
};

export default StockCardInfo;
