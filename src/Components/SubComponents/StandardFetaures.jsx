import React from "react";
import styled from "styled-components";
import { Container, Row, Col } from "react-bootstrap";
import FeatureCardInfo from "../SubComponents/FeatureCardInfo";

import technologyImg from "../../assets/images/technology.svg";
import liquidityImg from "../../assets/images/liquidity.svg";
import regulationImg from "../../assets/images/regulation.svg";
import securityImg from "../../assets/images/security.svg";

const StandardFetauresContainer = styled.div`
  .featureTitle {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    line-height: 37px;
    text-align: center;
    color: #ffffff;
    margin-top: 82px;
    // margin-top: 112px;
    margin-bottom: 96px;
  }
  .featuresRow .featureCardBox:nth-child(even) {
    flex-direction: row-reverse;
    .featureImg {
      margin-left: 7rem;
    }
  }
`;
const StandardFetaures = () => {
  const featuresDetails = [
    {
      Title: "Technology",
      Heading:
        "LMEX Technology is robust, battle-tested and ready to handle rapid scaling.",
      Text: "The platform can handle the maturity and complexity of trading system observed in traditional financial market such as forex, equities and commodities.",
      Img: technologyImg,
    },
    {
      Title: "Liquidity",
      Heading: "Our platform has participation from institutional traders",
      Text: "Traders with advanced capabilities to consolidate market data, produce aggregated order books and route order flow - thus providing one of the most liquid order books in the industry by far.",
      Img: liquidityImg,
    },
    {
      Title: "Regulation",
      Heading:
        "LMEX has compliance with global AML/KYC, funding source and other compliance measures.",
      Text: "Cryptocurrency exchange have been known to do away with such measures for a very long time and some have only now begun to comply with such policies",
      Img: regulationImg,
    },
    {
      Title: "Security",
      Heading:
        "All funds deposited on to the platform are secured and insured.",
      Text: "All funds are secured and insured by Industry-leading custodians who also guarantee risk-free settlements. We work with cybersecurity companies who regularly perform penetration testing across the app and servers. This is to ensure integrity of all systems and data.",
      Img: securityImg,
    },
  ];
  return (
    <StandardFetauresContainer>
      <Container>
        <Row>
          <Col>
            <div className="d-flex justify-content-center align-items-center  featureTitle">
              With Leading Industry Standard Features
            </div>
          </Col>
        </Row>
        <Row>
          <Col className="featuresRow">
            {featuresDetails.map((features, index) => (
              <FeatureCardInfo
                key={index}
                featureTitle={features.Title}
                featureHeading={features.Heading}
                featureText={features.Text}
                featureImage={features.Img}
              />
            ))}
          </Col>
        </Row>
      </Container>
    </StandardFetauresContainer>
  );
};

export default StandardFetaures;
