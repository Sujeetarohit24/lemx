import React from "react";
import styled from "styled-components";
import { Container, Row, Col, Button } from "react-bootstrap";
import { FaChevronRight } from "react-icons/fa";
import StandardFetaures from "./StandardFetaures";
import bannerImg from "../../assets/images/home-banner.svg";
const TopBannerContainer = styled.div`
  .bannerContent {
    max-width: 830px;
    margin: 0 auto;
    margin-top: 80px;
    .bannerHeading {
      font-family: "DIN Alternate";
      font-style: normal;
      font-weight: bold;
      font-size: 42px;
      line-height: 49px;
      text-align: center;
      color: #ffffff;
      margin-bottom: 23px;
    }
    .color1 {
      color: #006de4;
    }
    .color2 {
      color: #00b2a8;
    }
    .color3 {
      color: #3ed2cd;
    }
    .color4 {
      color: #73d0eb;
    }
    .bannerSubHeading {
      font-family: "Poppins";
      font-style: normal;
      font-weight: 400;
      font-size: 16px;
      line-height: 24px;
      text-align: center;
      color: #798595;
      max-width: 461px;
      margin: 0 auto;
    }
    .launchAppBtn {
      display: flex;
      justify-content: center;
      align-items: center;
      padding: 13px 16.6px;
      background: #006de4;
      border-radius: 8px;
      color: #ffffff;
      border: none;
      &:hover,
      &:focus,
      &:not(:disabled):not(.disabled):active {
        color: #ffffff;
        background: #006de4;
      }
    }
  }
  .bannerImageBox {
    margin-top: 90px;
    // background: url(${bannerImg}) no-repeat;
    // width: 100%;
    // height: 651px;
    // background-size: contain;
  }
`;
const TopBanner = () => {
  return (
    <TopBannerContainer>
      <Row>
        <Col>
          <div className="bannerContent">
            <h1 className="bannerHeading">
              A one-stop exchange platform for <br />
              <span className="color1">Cryptocurrency</span> .
              <span className="color2">Forex</span> .
              <span className="color3">Equities</span> .
              <span className="color4">Commodities</span>
            </h1>
            <div className="bannerSubHeading">
              Buy and sell 100+ cryptocurrencies with 20+ fiat currencies using
              bank transfer or you credit/debit card.
            </div>
            <div className="d-flex justify-content-center align-items-center mt-4">
              <Button variant="outline-secondary" className="launchAppBtn">
                <span>Launch App</span>
                <FaChevronRight className="ml-3" />
              </Button>
            </div>
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <div className="bannerImageBox">
            <img
              src={bannerImg}
              alt="Home Banner Image"
              className="img-fluid "
            />
          </div>
        </Col>
      </Row>
    </TopBannerContainer>
  );
};

export default TopBanner;
