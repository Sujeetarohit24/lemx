import React from "react";

import styled from "styled-components";
const FeatureCardBox = styled.div`
  max-width: 1130px;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 150px;
  .topHeading {
    font-family: "Poppins";
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 24px;
    color: #00b2a8;
  }
  .blockHeading {
    font-family: DIN Alternate;
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    line-height: 37px;
    color: #ffffff;
  }
  .featureText {
    font-family: "Poppins";
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 24px;
    color: #798595;
  }
`;
const FeatureImgBox = styled.div`
  margin-right: 5rem;
`;
const FeatureCardInner = styled.div`
  max-width: 555px;
`;
const FeatureCardInfo = ({
  featureTitle,
  featureHeading,
  featureText,
  featureImage,
}) => {
  return (
    <>
      <FeatureCardBox className="d-flex justify-content-between align-items-center featureCardBox">
        <FeatureCardInner>
          <p className="topHeading">{featureTitle}</p>
          <p className="blockHeading">{featureHeading}</p>
          <p className="featureText">{featureText}</p>
        </FeatureCardInner>
        <FeatureImgBox className="featureImg">
          <img src={featureImage} />
        </FeatureImgBox>
      </FeatureCardBox>
    </>
  );
};

export default FeatureCardInfo;
