import React from "react";

import { Row, Col } from "react-bootstrap";
import styled from "styled-components";

const PageHeadingInfo = styled.div`
  .pageTitleText {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 42px;
    line-height: 49px;
    text-align: center;
    color: #ffffff;
    margin-top: 85px;
    margin-bottom: 16px;
  }
  .pageSubtitleText {
    font-family: "Poppins";
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 24px;
    color: #798595;
    max-width: 46%;
    margin: 0 auto;
  }
`;
const PageHeading = ({ pageTitle, pageSubTitle, className }) => {
  return (
    <PageHeadingInfo>
      <Row>
        <Col>
          <h1 className="pageTitleText">{pageTitle}</h1>
          <p className="text-center mb-5 pageSubtitleText">{pageSubTitle}</p>
        </Col>
      </Row>
    </PageHeadingInfo>
  );
};

export default PageHeading;
