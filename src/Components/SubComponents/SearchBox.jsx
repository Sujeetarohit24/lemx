import React from "react";
import styled from "styled-components";
import { BsSearch } from "react-icons/bs";
const SearchBoxContent = styled.div`
  position: relative;
  max-width: 670px;
  margin: 0 auto;
  margin-bottom: 100px;
`;
const SearchBox = () => {
  return (
    <SearchBoxContent>
      <div className="form-group has-search">
        <span className="fa fa-search form-control-feedback">
          <BsSearch />
        </span>
        <input
          type="text"
          className="form-control"
          placeholder="How can we help you?"
        />
      </div>
    </SearchBoxContent>
  );
};

export default SearchBox;
