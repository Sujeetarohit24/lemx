import React from "react";

import { Row, Col } from "react-bootstrap";
import styled from "styled-components";

const TradingVolumeBoxContainer = styled.div`
  max-width: 800px;
  margin: 0 auto;
  background: linear-gradient(180deg, #223147 0%, rgba(34, 49, 71, 0.1) 100%);
  border: 1px solid rgba(255, 255, 255, 0.1);
  box-sizing: border-box;
  backdrop-filter: blur(100px);
  /* Note: backdrop-filter has minimal browser support */
  border-radius: 28px;
  padding: 53.5px 143px;
  margin-bottom: 120px;
  .tradingValue {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 82px;
    line-height: 95px;
    text-align: center;
    letter-spacing: 0.02em;
    color: #ffffff;
  }
  .tradeSubText {
    font-family: "Poppins";
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    line-height: 30px;
    text-align: center;
    color: #798595;
  }
`;
const TradeVolumeBox = ({ tradeValue, tradeVolume }) => {
  return (
    <Row>
      <Col>
        <TradingVolumeBoxContainer>
          <div className="tradingValue">{tradeValue}</div>
          <p className="tradeSubText">{tradeVolume}</p>
        </TradingVolumeBoxContainer>
      </Col>
    </Row>
  );
};

export default TradeVolumeBox;
