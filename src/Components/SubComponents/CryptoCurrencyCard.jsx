import React from "react";
import { Link } from "react-router-dom";
import { Row, Col, Card } from "react-bootstrap";
import styled from "styled-components";
import { BsTriangleFill } from "react-icons/bs";
import { FaChevronRight } from "react-icons/fa";
import bnbIcon from "../../assets/images/bnb-icon.svg";
import usdtIcon from "../../assets/images/usdt-icon.svg";
import dotIcon from "../../assets/images/dot-icon.svg";

const CryptoCurrnecyCardContainer = styled.div``;

const CryptoCard = styled(Card)`
  background: linear-gradient(
    180deg,
    rgba(34, 49, 71, 0.7) 0%,
    rgba(34, 49, 71, 0.07) 100%
  );
  border: 1px solid rgba(255, 255, 255, 0.2);
  box-sizing: border-box;
  box-shadow: 0px 0px 15px rgba(255, 255, 255, 0.07);
  backdrop-filter: blur(100px);
  border-radius: 8px;
  padding: 20px;
  min-height: 130px;
  height: 100%;

  .cryptoCardTitle {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
    color: #ffffff;
    margin-bottom: 31px;
  }
  .cryptoCardSubTitle {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 14px;
    color: #798595;
  }
  .cryptoCurrencyTitle {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
    color: #ffffff;
    margin-bottom: 3px;
  }
  .cryptoTradeValue {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
  }

  .cryptoTradeMark {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    line-height: 19px;
    color: #ffffff;
  }
  .crtptoTradeVolume {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 21px;
    text-align: right;
    color: #798595;
  }
  .allCard {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    color: #006de4;
  }
`;
const CryptoCurrencyCard = () => {
  const cryptoCurrencyInfo = [
    {
      iconImg: bnbIcon,
      cryptoTradeType: "Ethereum",
      cryptoTradeTypeShortCode: "ETH",
      cryptoTradeStockState: BsTriangleFill,
      cryptoStockValue: "1.40%",
      cryptoTradeMarkValue: "$2,845.37",
      cryptoTradeVolume: "VOL: $735.4M",
    },
    {
      iconImg: bnbIcon,
      cryptoTradeType: "Bitcoin",
      cryptoTradeTypeShortCode: "BTC",
      cryptoTradeStockState: BsTriangleFill,
      cryptoStockValue: "1.02%",
      cryptoTradeMarkValue: "$41,488.00",
      cryptoTradeVolume: "VOL: $724.3M",
    },
    {
      iconImg: usdtIcon,
      cryptoTradeType: "Cardano",
      cryptoTradeTypeShortCode: "ADA",
      cryptoTradeStockState: BsTriangleFill,
      cryptoStockValue: "0.28%",
      cryptoTradeMarkValue: "$2.05",
      cryptoTradeVolume: "VOL: $8.5M",
    },
    {
      iconImg: dotIcon,
      cryptoTradeType: "Bitcoin Cash",
      cryptoTradeTypeShortCode: "BCH",
      cryptoTradeStockState: BsTriangleFill,
      cryptoStockValue: "1.82%",
      cryptoTradeMarkValue: "$485.37",
      cryptoTradeVolume: "VOL: $9.1M",
    },
    {
      iconImg: dotIcon,
      cryptoTradeType: "Bitcoin Cash",
      cryptoTradeTypeShortCode: "BCH",
      cryptoTradeStockState: BsTriangleFill,
      cryptoStockValue: "1.82%",
      cryptoTradeMarkValue: "$485.37",
      cryptoTradeVolume: "VOL: $9.1M",
    },
  ];
  return (
    <CryptoCurrnecyCardContainer>
      <Row>
        {cryptoCurrencyInfo.map((cryptoCurrency, index) => (
          <Col sm={3} key={index}>
            <div className="mb30">
              <CryptoCard>
                <Card.Body className="p-0">
                  <Card.Text className="d-flex align-items-center cryptoCardTitle">
                    <img
                      src={cryptoCurrency.iconImg}
                      alt="Crypto Currency Icon"
                    />
                    <div className="ml-2">
                      <div className="cryptoCurrencyTitle">
                        {cryptoCurrency.cryptoTradeType}
                      </div>
                      <div className="cryptoCardSubTitle">
                        {cryptoCurrency.cryptoTradeTypeShortCode}
                      </div>
                    </div>
                    <div className="d-flex align-items-center ml-auto cryptoTradeValue greenText">
                      <span className="upTriangle">
                        <cryptoCurrency.cryptoTradeStockState />
                      </span>
                      <span className="ml-1">
                        {cryptoCurrency.cryptoStockValue}
                      </span>
                    </div>
                  </Card.Text>
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="cryptoTradeMark">
                      {cryptoCurrency.cryptoTradeMarkValue}
                    </div>
                    <div className="crtptoTradeVolume">
                      {cryptoCurrency.cryptoTradeVolume}
                    </div>
                  </div>
                </Card.Body>
              </CryptoCard>
            </div>
          </Col>
        ))}
        <Col sm={3}>
          <div className="mb30">
            <Link to="/">
              <CryptoCard className="d-flex justify-content-center align-items-center ">
                <div className="d-flex justify-content-center align-items-center allCard">
                  <span>Explore All Coins</span>
                  <FaChevronRight className="ml-3" />
                </div>
              </CryptoCard>
            </Link>
          </div>
        </Col>
      </Row>
    </CryptoCurrnecyCardContainer>
  );
};

export default CryptoCurrencyCard;
