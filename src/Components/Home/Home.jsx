import React from "react";
import styled from "styled-components";
import { Container, Row, Col } from "react-bootstrap";
import CryptoCurrencyCard from "../SubComponents/CryptoCurrencyCard";
import StandardFetaures from "../SubComponents/StandardFetaures";
import TopBanner from "../SubComponents/TopBanner";
import StockCardInfo from "../SubComponents/StockCardInfo";
import ForexCard from "../SubComponents/ForexCard";
import TradeVolumeBox from "../SubComponents/TradeVolumeBox";

import PageFooter from "../PageFooter";
import PageSubFooter from "../PageSubFooter";

import kronosLogo from "../../assets/images/kronos-logo.svg";
import spartanLogo from "../../assets/images/spartan-logo.svg";
import sixtantLogo from "../../assets/images/sixtant-logo.svg";
import delphiDigitalLogo from "../../assets/images/delphi-digital-logo.svg";

const HomePageContainer = styled.div`
  .bigHeading {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    line-height: 37px;
    text-align: center;
    color: #ffffff;
    margin-top: -100px;
  }
  .sectionHeading {
    font-family: "Poppins";
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    line-height: 30px;
    color: #ffffff;
    margin-bottom: 36px;
  }
`;
const TrustedPartnerBox = styled.div`
  max-width: 755px;
  margin: 0 auto;
  .trustedPartnerTitle {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    line-height: 37px;
    text-align: center;
    color: #ffffff;
    margin-bottom: 60px;
  }
`;
const Home = () => {
  return (
    <HomePageContainer>
      <Container>
        <TopBanner />
        <Row>
          <Col>
            <div className="bigHeading text-white">
              A Wider Range of Markets to Trade In
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="sectionHeading">
              Buy, sell and use <strong>Cryptocurrencies</strong>
            </div>
          </Col>
        </Row>
        <CryptoCurrencyCard />
        <Row>
          <Col>
            <div className="sectionHeading">
              Trade in the most active <strong>Stocks </strong>right now
            </div>
          </Col>
        </Row>
        <StockCardInfo />
        <Row>
          <Col>
            <div className="sectionHeading">
              <strong>Forex</strong> is the most trading market in the world
            </div>
          </Col>
        </Row>
        <ForexCard />
        <StandardFetaures />
        <TradeVolumeBox
          tradeValue="$8,953,700,100"
          tradeVolume="Trading Volume in last 24h"
        />
        <TrustedPartnerBox>
          <div className="trustedPartnerTitle">Trusted Partners/Licenses</div>
          <div className="d-flex justify-content-between align-items-center">
            <img src={kronosLogo} alt="Kronos Logo" className="img-fluid" />
            <img src={spartanLogo} alt="Spartan Logo" className="img-fluid" />
            <img src={sixtantLogo} alt="Sixtant Logo" className="img-fluid" />
            <img
              src={delphiDigitalLogo}
              alt="Delphi Digital Logo"
              className="img-fluid"
            />
          </div>
        </TrustedPartnerBox>

        <PageSubFooter />
        <PageFooter />
      </Container>
    </HomePageContainer>
  );
};

export default Home;
