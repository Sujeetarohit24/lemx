import React from "react";
import { Container, Row, Col, ListGroup } from "react-bootstrap";
import styled from "styled-components";
import lmexWhiteLogo from "../assets/images/lmex-logo-white.svg";
const PageSubFooterBox = styled.div`
  margin-top: 80px;
  .footerLeftBox {
    p {
      font-family: "Poppins";
      font-style: normal;
      font-weight: 400;
      font-size: 16px;
      line-height: 24px;
      color: #798595;
      // text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    }
  }
  .footerLinks {
    .list-group-item {
      background: transparent;
      padding: 0;
      margin-bottom: 22px;
      border: none;
      a {
        font-family: "Poppins";
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 21px;
        color: #798595;
        text-decoration: none;
        &:hover,
        &:focus {
          color: #ffffff;
        }
      }
    }
  }
`;
const PageSubFooter = () => {
  return (
    <PageSubFooterBox>
      <Container>
        <Row>
          <Col md={5}>
            <div className="w-75">
              <img src={lmexWhiteLogo} className="img-fluid mb-4" />
              <div className="footerLeftBox">
                <p className="mb-4">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>

                <div className="">
                  <p>
                    info@lmex.com
                    <br />
                    +91 987 654 3210
                  </p>
                </div>
              </div>
            </div>
          </Col>
          <Col md={7}>
            <div className="d-flex justify-content-between align-items-center footerLinks">
              <ListGroup className="">
                <ListGroup.Item>
                  <a href="#">Products</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">Market</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">Top 10 Crypto</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">Top 10 Stocks</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">Forex Trading</a>
                </ListGroup.Item>
              </ListGroup>

              <ListGroup>
                <ListGroup.Item>
                  <a href="#">Fees</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">White paper</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">API Documentation</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">Data Policy</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">Terms &amp; Conditions</a>
                </ListGroup.Item>
              </ListGroup>

              <ListGroup>
                <ListGroup.Item>
                  <a href="#">About LMEX</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">Create Account</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">Help &amp; Support</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">Contact us</a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">Careers</a>
                </ListGroup.Item>
              </ListGroup>
            </div>
          </Col>
        </Row>
      </Container>
    </PageSubFooterBox>
  );
};

export default PageSubFooter;
