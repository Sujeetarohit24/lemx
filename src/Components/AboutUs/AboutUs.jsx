import React from "react";

import { Container, Row, Col, Card } from "react-bootstrap";
import styled from "styled-components";

import PageHeading from "../SubComponents/PageHeading";
import StandardFetaures from "../SubComponents/StandardFetaures";

import PageFooter from "../PageFooter";
import PageSubFooter from "../PageSubFooter";
import TradeVolumeBox from "../SubComponents/TradeVolumeBox";
const AboutUsContainer = styled.div``;

const CardInfo = styled(Card)`
  background: linear-gradient(180deg, #223147 0%, rgba(34, 49, 71, 0.1) 100%);
  border: 1px solid rgba(255, 255, 255, 0.1);
  box-sizing: border-box;
  backdrop-filter: blur(100px);
  /* Note: backdrop-filter has minimal browser support */
  border-radius: 16px;
  padding: 40px;
  height: 100%;
  .cardTitle {
    font-family: DIN Alternate;
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    line-height: 37px;
    color: #ffffff;
  }
  .cardText {
    font-family: Poppins;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 24px;
    color: #798595;
  }
`;

const Aboutus = () => {
  return (
    <AboutUsContainer>
      <Container>
        <div className="pb-2">
          <PageHeading
            pageTitle="About LMEX"
            pageSubTitle="Beyond operating the world's leading cryptocurrency exchange, LMEX spans an entire ecosystem."
          />
        </div>
        <Row className="row-smaller mt-4">
          <Col sm={6}>
            <CardInfo>
              <Card.Body>
                <Card.Title className="cardTitle">Who we are?</Card.Title>
                <Card.Text className="cardText">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                  irure dolor in reprehenderit in voluptate velit esse cillum
                  dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                  cupidatat non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.
                </Card.Text>
              </Card.Body>
            </CardInfo>
          </Col>
          <Col sm={6}>
            <CardInfo>
              <Card.Body>
                <Card.Title className="cardTitle">
                  What We Believe In
                </Card.Title>
                <Card.Text className="cardText">
                  Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                  accusantium doloremque laudantium, totam rem aperiam, eaque
                  ipsa quae ab illo inventore veritatis et quasi architecto
                  beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                  quia voluptas sit aspernatur aut odit aut fugit, sed quia
                  consequuntur magni dolores eos qui ratione voluptatem sequi
                  nesciunt.
                </Card.Text>
              </Card.Body>
            </CardInfo>
          </Col>
        </Row>
        <StandardFetaures />
        <TradeVolumeBox
          tradeValue="$8,953,700,100"
          tradeVolume="Trading Volume in last 24h"
        />
        <PageSubFooter />
        <PageFooter />
      </Container>
    </AboutUsContainer>
  );
};

export default Aboutus;
