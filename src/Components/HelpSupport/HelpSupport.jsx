import React, { useState } from "react";
import { Container, Row, Col, Accordion, Card } from "react-bootstrap";

import { FaAngleRight } from "react-icons/fa";
import styled from "styled-components";

import PageFooter from "../PageFooter";
import PageHeading from "../SubComponents/PageHeading";
import SearchBox from "../SubComponents/SearchBox";

import flagIconImg from "../../assets/images/flag-icon.svg";
import walletIconImg from "../../assets/images/wallet-icon.svg";
import tagIconImg from "../../assets/images/tag-icon.svg";
import arrowIconImg from "../../assets/images/arrow-icon.svg";
import bookIconImg from "../../assets/images/book-icon.svg";
import fileIconImg from "../../assets/images/file-icon.svg";
import upArrowImg from "../../assets/images/up-arrow-icon.svg";
import downArrowImg from "../../assets/images/down-arrow-icon.svg";

const HelpSupportContainer = styled.div``;
const AccordionBox = styled.div`
  max-width: 1230px;
  margin: 0 auto;
  margin-bottom: 100px;
  .accordionInnerBox {
    background: linear-gradient(
      180deg,
      rgba(34, 49, 71, 0.7) 0%,
      rgba(34, 49, 71, 0) 100%
    );
    backdrop-filter: blur(100px);
    border-radius: 8px;
    margin-bottom: 30px;
    border: none;
  }
  .cardHeadingTitle {
    padding: 0;
    // border: 1px solid rgba(0, 109, 228, 0.7);
    border-radius: 8px 8px 0 0 !important;
    border: none;
    .title {
      font-family: "DIN Alternate";
      font-style: normal;
      font-weight: bold;
      font-size: 24px;
      line-height: 28px;
      color: #ffffff;
      margin-bottom: 5px;
    }
    .subTitle {
      font-family: Poppins;
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 14px;
      color: #798595;
    }
  }
`;
const FeatureCard = styled.div`
  padding: 20px;
  margin-bottom: 30px;
  background: rgba(31, 43, 62, 0.9);
  backdrop-filter: blur(100px);
  border-radius: 8px;
  .mediaContent {
    .mediaContentTitle {
      font-family: "Poppins";
      font-style: normal;
      font-weight: 500;
      font-size: 16px;
      line-height: 24px;
      color: #ffffff;
    }
    .mediaContentSubTitle {
      font-family: "Poppins";
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 21px;
      color: #798595;
    }
  }
`;
const HelpSupport = () => {
  const contentCardInfo = [
    {
      iconImg: flagIconImg,
      title: "Start Here",
      subTitle: "7 Articles",
    },
    {
      iconImg: walletIconImg,
      title: "Accounts & Wallets",
      subTitle: "7 Articles",
    },
    {
      iconImg: tagIconImg,
      title: "Developers",
      subTitle: "7 Articles",
    },
    {
      iconImg: arrowIconImg,
      title: "What’s Next?",
      subTitle: "7 Articles",
    },
    {
      iconImg: bookIconImg,
      title: "Guides & Resources",
      subTitle: "7 Articles",
    },
    {
      iconImg: fileIconImg,
      title: "Legal",
      subTitle: "7 Articles",
    },
  ];

  const [arrow, setArrow] = useState("");
  const [isRevealArrow, setIsRevealArrow] = useState(false);
  return (
    <HelpSupportContainer>
      <Container>
        <div className="">
          <PageHeading pageTitle="Help & Support" />
        </div>
        <SearchBox />
        <Row>
          <Col>
            <AccordionBox>
              <Accordion defaultActiveKey="0">
                <Card className="accordionInnerBox">
                  <Card.Header className="cardHeadingTitle">
                    <Accordion.Toggle
                      as={Card.Header}
                      eventKey="0"
                      onChange={(e) => setArrow(e.target.value)}
                      name="arrow"
                    >
                      <div className="d-flex justify-content-between align-items-center">
                        <div>
                          <div className="title">General</div>
                          <div className="subTitle">
                            Commonly asked questions about LMEX
                          </div>
                        </div>
                        <img
                          src={isRevealArrow ? downArrowImg : upArrowImg}
                          onClick={() =>
                            setIsRevealArrow((prevState) => !prevState)
                          }
                        />
                      </div>
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body>
                      <Container>
                        <Row>
                          {contentCardInfo.map(
                            (contentCard, contentCardIndex) => (
                              <Col sm={3} key={contentCardIndex}>
                                <FeatureCard className="d-flex">
                                  <img src={contentCard.iconImg} alt="Icon" />
                                  <div className="mediaContent ml-3">
                                    <div className="mediaContentTitle">
                                      {contentCard.title}
                                    </div>
                                    <div className="mediaContentSubTitle">
                                      {contentCard.subTitle}
                                    </div>
                                  </div>
                                </FeatureCard>
                              </Col>
                            )
                          )}
                        </Row>
                      </Container>
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card className="accordionInnerBox">
                  <Card.Header className="cardHeadingTitle">
                    <Accordion.Toggle
                      as={Card.Header}
                      eventKey="1"
                      onChange={(e) => setArrow(e.target.value)}
                      name="arrow"
                    >
                      <div className="d-flex justify-content-between align-items-center">
                        <div>
                          <div className="title">Perpetuals on L2</div>
                          <div className="subTitle">
                            FAQs on trading Perpetual Contracts on Layer 2
                          </div>
                        </div>
                        <img
                          src={isRevealArrow ? downArrowImg : upArrowImg}
                          onClick={() =>
                            setIsRevealArrow((prevState) => !prevState)
                          }
                        />
                      </div>
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="1">
                    <Card.Body>Hello! I'm another body</Card.Body>
                  </Accordion.Collapse>
                </Card>

                <Card className="accordionInnerBox">
                  <Card.Header className="cardHeadingTitle">
                    <Accordion.Toggle as={Card.Header} eventKey="2">
                      <div className="title">
                        Spot, Margin, Borrow, Lend on L1
                      </div>
                      <div className="subTitle">
                        FAQs on our suite of Layer 1 products using Ethereum
                      </div>
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="2">
                    <Card.Body>Hello! I'm another body</Card.Body>
                  </Accordion.Collapse>
                </Card>

                <Card className="accordionInnerBox">
                  <Card.Header className="cardHeadingTitle">
                    <Accordion.Toggle as={Card.Header} eventKey="3">
                      <div className="title">(Old) Perpetuals on L1</div>
                      <div className="subTitle">
                        FAQs on trading Perpetuals on Layer 1, which is now
                        offline
                      </div>
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="3">
                    <Card.Body>Hello! I'm another body</Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            </AccordionBox>
          </Col>
        </Row>
      </Container>
      <PageFooter />
    </HelpSupportContainer>
  );
};
export default HelpSupport;
