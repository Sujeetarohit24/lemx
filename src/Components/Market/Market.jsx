import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  Tab,
  Nav,
  Button,
  Table,
} from "react-bootstrap";

import styled from "styled-components";
import PageHeading from "../SubComponents/PageHeading";
import PageSubFooter from "../PageSubFooter";
import PageFooter from "../PageFooter";
import { BsTriangleFill, BsStar } from "react-icons/bs";
import bnbIcon from "../../assets/images/bnb-icon.svg";
import usdtIcon from "../../assets/images/usdt-icon.svg";
import dotIcon from "../../assets/images/dot-icon.svg";
import graphIconImg from "../../assets/images/graph-icon.svg";
import bitcoin1Icon from "../../assets/images/bitcoin1-icon.svg";
import bitcoin2Icon from "../../assets/images/bitcoin2-icon.svg";
import bitcoin3Icon from "../../assets/images/bitcoin3-icon.svg";

const MarketContainer = styled.div`
  .bigTitle {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    line-height: 37px;
    color: #ffffff;
    margin-top: 62px;
    margin-bottom: 23px;
  }
`;

const TradeCard = styled(Card)`
  background: linear-gradient(180deg, #223147 0%, rgba(34, 49, 71, 0.1) 100%);
  border: 1px solid rgba(255, 255, 255, 0.2);
  box-sizing: border-box;
  backdrop-filter: blur(100px);
  /* Note: backdrop-filter has minimal browser support */
  border-radius: 8px;
  padding: 20px;
  .topTitle {
    font-family: "Poppins";
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 21px;
    color: #798595;
  }
  .cardText {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
    color: #ffffff;
    margin-bottom: 22px;
  }
  .tradeTitle {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
    color: #ffffff;
  }
  .tradeValue {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
  }

  .tradeMark {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
    color: #ffffff;
  }
  .tradeVolume {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 21px;
    text-align: right;
    color: #798595;
  }
`;
const TradeTableInfo = styled.div`
  .tradeTable {
    .tradeTableHead {
      background: rgba(31, 43, 62, 0.9);
      border-radius: 8px;
      overflow: hidden;
      th {
        font-family: "Poppins";
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 21px;
        color: #798595;
        border: none;
        padding: 14.25px 12px;
        &:first-child {
          border-radius: 8px 0 0 8px;
        }
        &:last-child {
          border-radius: 0 8px 8px 0;
        }
      }
    }
    .tradeTableBody {
      tr:first-child {
        td {
          border-top: none;
        }
      }
      td {
        font-family: "DIN Alternate";
        font-style: normal;
        font-weight: bold;
        font-size: 18px;
        line-height: 21px;
        color: #ffffff;
        vertical-align: middle;
        min-height: 80px;
        border-bottom: 1px solid #394556;
      }
      .tradeCompany {
        font-family: "Poppins";
        font-style: normal;
        font-weight: 400;
        font-size: 18px;
        line-height: 27px;
        color: #798595;
      }
    }
  }
`;
const Market = () => {
  const tradeInfo = [
    {
      tradeTitle: "Top Gainer",
      iconImg: bnbIcon,
      tradeType: "BNBUP/USDT",
      stockState: BsTriangleFill,
      stockValue: "23.15%",
      tradeMarkValue: "$258.09",
      tradeVolume: "VOL: $6.8",
    },
    {
      tradeTitle: "Top Loser",
      iconImg: bnbIcon,
      tradeType: "BNBDOWN/USDT",
      stockState: BsTriangleFill,
      stockValue: "21.15%",
      tradeMarkValue: "0.4381",
      tradeVolume: "VOL: $6.8",
    },
    {
      tradeTitle: "Top Volume",
      iconImg: usdtIcon,
      tradeType: "USDT/BIDR",
      stockState: BsTriangleFill,
      stockValue: "0.01%",
      tradeMarkValue: "14,370",
      tradeVolume: "VOL: $31.2",
    },
    {
      tradeTitle: "Highlight",
      iconImg: dotIcon,
      tradeType: "DOT/USDT",
      stockState: BsTriangleFill,
      stockValue: "1.25%",
      tradeMarkValue: "26.81",
      tradeVolume: "VOL: $5.6",
    },
  ];
  // Trade table Info
  const tradeCellInfo = [
    {
      tradeImg: bitcoin1Icon,
      tradeCurrency: "Bitcoin",
      tradeCurrencyType: "BTC/USDT",
      lastPrice: "$41,209.33",
      changePrice: "1.45%",
      changeVolume: "$30,212.17M",
      marketCap: "$775,932.90M",
      chart: graphIconImg,
    },
    {
      tradeImg: bitcoin2Icon,
      tradeCurrency: "Bitcoin",
      tradeCurrencyType: "BTC/USDT",
      lastPrice: "$41,209.33",
      changePrice: "1.45%",
      changeVolume: "$30,212.17M",
      marketCap: "$775,932.90M",
      chart: graphIconImg,
    },
    {
      tradeImg: bitcoin3Icon,
      tradeCurrency: "Bitcoin",
      tradeCurrencyType: "BTC/USDT",
      lastPrice: "$41,209.33",
      changePrice: "1.45%",
      changeVolume: "$30,212.17M",
      marketCap: "$775,932.90M",
      chart: graphIconImg,
    },
  ];
  return (
    <MarketContainer>
      <Container>
        <PageHeading pageTitle="Market" />
        <Row className="row-extrasmaller">
          {tradeInfo.map((trade, index) => (
            <Col sm={3} key={index}>
              <TradeCard>
                <Card.Body className="p-0">
                  <Card.Title className="topTitle">
                    {trade.tradeTitle}
                  </Card.Title>
                  <Card.Text className="d-flex align-items-center cardText">
                    <img src={trade.iconImg} alt="Trade Icon" />
                    <div className="ml-2 tradeTitle">{trade.tradeType}</div>
                    <div className="d-flex align-items-center ml-auto tradeValue greenText">
                      <span className="upTriangle">
                        <trade.stockState />
                      </span>
                      <span className="ml-1">{trade.stockValue}</span>
                    </div>
                  </Card.Text>
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="tradeMark">{trade.tradeMarkValue}</div>
                    <div className="tradeVolume">{trade.tradeVolume}</div>
                  </div>
                </Card.Body>
              </TradeCard>
            </Col>
          ))}
        </Row>
        <Row>
          <Col>
            <div className="text-center bigTitle">
              A wider range of markets to trade in
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <Tab.Container defaultActiveKey="first">
              <div className="marketTabsInfo">
                <Nav className="d-flex justify-content-center align-items-center marketTabs">
                  <Nav.Item>
                    <Nav.Link eventKey="first">Crytpo</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="second">Stocks</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="third">Forex</Nav.Link>
                  </Nav.Item>
                </Nav>

                <Tab.Content className="text-white">
                  <Tab.Pane eventKey="first">
                    <div className="d-flex justify-content-center align-items-center buttonBox">
                      <Button variant="outline-secondary" className="filterBtn">
                        All
                      </Button>
                      <Button
                        variant="outline-secondary"
                        className="grayOutlineBtn"
                      >
                        Top
                      </Button>
                      <Button
                        variant="outline-secondary"
                        className="grayOutlineBtn"
                      >
                        GameFi
                      </Button>
                      <Button
                        variant="outline-secondary"
                        className="grayOutlineBtn"
                      >
                        Meme
                      </Button>
                      <Button
                        variant="outline-secondary"
                        className="grayOutlineBtn"
                      >
                        Polkadot
                      </Button>
                      <Button
                        variant="outline-secondary"
                        className="grayOutlineBtn"
                      >
                        NFT
                      </Button>
                      <Button
                        variant="outline-secondary"
                        className="grayOutlineBtn"
                      >
                        DeFi
                      </Button>
                      <Button
                        variant="outline-secondary"
                        className="grayOutlineBtn"
                      >
                        Layer 2
                      </Button>
                      <Button
                        variant="outline-secondary"
                        className="grayOutlineBtn"
                      >
                        Grayscale
                      </Button>
                    </div>
                    <TradeTableInfo>
                      <Table striped hover className="tradeTable">
                        <thead className="tradeTableHead">
                          <tr>
                            <th>Currency</th>
                            <th>Last Price</th>
                            <th>24h Change</th>
                            <th>24h Volume</th>
                            <th>Market Cap</th>
                            <th>Chart</th>
                            <th className="text-right">Trade</th>
                          </tr>
                        </thead>

                        <tbody className="tradeTableBody">
                          {tradeCellInfo.map((tradeCell, index) => (
                            <tr>
                              <td>
                                <div className="d-flex align-items-center">
                                  <span>
                                    <BsStar />
                                  </span>
                                  <img
                                    src={tradeCell.tradeImg}
                                    alt="Trade Icon"
                                    className="img-fluid ml-2"
                                  />
                                  <div className="ml-2">
                                    {tradeCell.tradeCurrency}
                                    <span className="tradeCompany ml-2">
                                      {tradeCell.tradeCurrencyType}
                                    </span>
                                  </div>
                                </div>
                              </td>
                              <td>{tradeCell.lastPrice}</td>
                              <td>
                                <div className="d-flex align-items-center greenText">
                                  <span className="upTriangle">
                                    <BsTriangleFill />
                                  </span>
                                  <span className="ml-1">
                                    {tradeCell.changePrice}
                                  </span>
                                </div>
                              </td>
                              <td>{tradeCell.changeVolume}</td>
                              <td>{tradeCell.marketCap}</td>
                              <td>
                                <img src={tradeCell.chart} alt="Graph Icon" />
                              </td>
                              <td className="text-right">
                                <Button className="buySaleBtn">Buy/Sell</Button>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </Table>
                      <div className="text-center">
                        <Button
                          variant="outline-secondary"
                          className="loadMoreBtn"
                        >
                          Load More
                        </Button>
                      </div>
                    </TradeTableInfo>
                  </Tab.Pane>
                  <Tab.Pane eventKey="second">tab panel content 2</Tab.Pane>
                  <Tab.Pane eventKey="third">tab panel content 3</Tab.Pane>
                </Tab.Content>
              </div>
            </Tab.Container>
          </Col>
        </Row>
        <PageSubFooter />
        <PageFooter />
      </Container>
    </MarketContainer>
  );
};

export default Market;
