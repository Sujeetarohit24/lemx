import React, { useState } from "react";
import { Form, Button } from "react-bootstrap";
import { FaAngleRight } from "react-icons/fa";
import styled from "styled-components";

import showPwdImg from "../../assets/images/eye-line.svg";
import hidePwdImg from "../../assets/images/eye-hide-line.svg";
import PageFooter from "../PageFooter";

const SignupFormBox = styled.div`
  h1 {
    font-family: "DIN Alternate";
    font-style: normal;
    font-weight: bold;
    font-size: 42px;
    line-height: 49px;
    color: #ffffff;
    margin-top: 28px;
    margin-bottom: 33px;
  }

  .signupFormContent {
    max-width: 520px;
    margin: 0 auto;
    padding: 30px 40px 46px 40px;
    background: linear-gradient(180deg, #223147 0%, rgba(34, 49, 71, 0.1) 100%);
    border: 1px solid rgba(255, 255, 255, 0.1);
    box-sizing: border-box;
    backdrop-filter: blur(100px);
    border-radius: 16px;
    margin-bottom: 80px;
    h2 {
      font-family: "Poppins";
      font-style: normal;
      font-weight: 500;
      font-size: 24px;
      line-height: 36px;
      text-align: center;
      color: #ffffff;
    }

    .formControl {
      background: rgba(255, 255, 255, 0.05);
      border: 1px solid rgba(255, 255, 255, 0.2);
      box-sizing: border-box;
      border-radius: 8px;
      color: #ffffff;
      height: calc(2.5em + 0.75rem + 4px);
    }
    .custom-control-label {
      font-family: "Poppins";
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 21px;
      color: #798595 !important;
    }
    .formGroup {
      margin-bottom: 20px;
    }
    .passWrapper {
      position: relative;
      display: flex;
      margin-bottom: 14px;
      img {
        position: absolute;
        top: 38%;
        right: 4%;
      }
      img:hover {
        cursor: pointer;
      }
    }
    .signupBox {
      @media (max-width: 767px) {
        display: block !important;
      }
      .signupBoxContent {
        @media (max-width: 767px) {
          display: block !important;
        }
      }
    }
  }
`;

const SignupForm = () => {
  const [pwd, setPwd] = useState("");
  const [isRevealPwd, setIsRevealPwd] = useState(false);

  const [confirmPwd, setConfirmPwd] = useState("");
  const [isRevealConfirmPwd, setIsRevealConfirmPwd] = useState(false);
  return (
    <>
      <SignupFormBox>
        <h1 className="text-center">Create your account</h1>

        <div className="signupFormContent">
          <h2 className="text-center">Start Trading!</h2>
          <Form>
            <Form.Group className="formGroup" controlId="formBasicEmail">
              <Form.Control
                type="email"
                placeholder="Email ID"
                className="formControl"
                autoComplete="off"
              />
              {/* <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text> */}
            </Form.Group>

            <Form.Group className="formGroup" controlId="formBasicPassword">
              <div className="passWrapper">
                <Form.Control
                  name="pwd"
                  type={isRevealPwd ? "text" : "password"}
                  value={pwd}
                  placeholder="Password"
                  className="formControl"
                  autoComplete="off"
                  onChange={(e) => setPwd(e.target.value)}
                />
                <img
                  title={isRevealPwd ? "Hide password" : "Show password"}
                  src={isRevealPwd ? hidePwdImg : showPwdImg}
                  onClick={() => setIsRevealPwd((prevState) => !prevState)}
                />
              </div>
            </Form.Group>
            <Form.Group
              className="formGroup"
              controlId="formBasicConfirmPassword"
            >
              <div className="passWrapper">
                <Form.Control
                  name="confirmPwd"
                  type={isRevealConfirmPwd ? "text" : "password"}
                  value={confirmPwd}
                  placeholder="Confirm Password"
                  className="formControl"
                  autoComplete="off"
                  onChange={(e) => setConfirmPwd(e.target.value)}
                />
                <img
                  title={isRevealConfirmPwd ? "Hide password" : "Show password"}
                  src={isRevealConfirmPwd ? hidePwdImg : showPwdImg}
                  onClick={() =>
                    setIsRevealConfirmPwd((prevState) => !prevState)
                  }
                />
              </div>
            </Form.Group>

            <Form.Group className="formGroup" controlId="formBasicCheckbox">
              <div class="custom-control custom-checkbox">
                <input
                  type="checkbox"
                  className="custom-control-input"
                  id="customCheck1"
                />
                <label
                  className="custom-control-label text-white"
                  for="customCheck1"
                >
                  I have read and agree to LMEX’s{" "}
                  <a href="#">Terms of Conditions</a> and{" "}
                  <a href="#">Data Policy</a>.
                </label>
              </div>
            </Form.Group>
            <Form.Group className="formGroup">
              <div className="d-flex justify-content-between align-items-center signupBox">
                <div className="d-flex flex-column signupBoxContent">
                  <p className="textLabel mb-1">Already have an account?</p>
                  <a href="#" className="text-white text-decoration-none">
                    Login here
                  </a>
                </div>
                <Button
                  variant="primary"
                  type="submit"
                  className="blueBtn ml-auto"
                >
                  Sign Up <FaAngleRight className="ml-3" />
                </Button>
              </div>
            </Form.Group>
          </Form>
        </div>
      </SignupFormBox>
      <PageFooter />
    </>
  );
};

export default SignupForm;
