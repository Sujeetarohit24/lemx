import React from "react";
import { Container, Row, Col, ListGroup } from "react-bootstrap";
import {
  FaTwitter,
  FaLinkedinIn,
  FaRedditAlien,
  FaGithub,
} from "react-icons/fa";
import styled from "styled-components";
const Footer = styled.div`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  border-top: 1px solid #394556;
  .copyrightText {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 21px;
    color: #798595;
    margin-bottom: 33px;
  }
  .footerSubText {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 18px;
    color: #798595;
  }
  .footerSocialInfo {
    @media (max-width: 767px) {
      flex-direction: column;
      margin-bottom: 20px;
    }
    .socialInfo {
      .list-group-item {
        background: transparent;
        border: none;
        color: #ffffff;
        padding: 0 0 0 42px;
        a {
          color: #ffffff;
          font-size: 22px;
        }
        &.active {
          a {
            color: #006de4;
          }
        }
      }
    }
  }
`;
const PageFooter = () => {
  return (
    <Container>
      <Row>
        <Col>
          <Footer>
            <div className="d-flex justify-content-between align-items-center mt-5 footerSocialInfo">
              <p className="copyrightText">
                &copy; 2021 LMEX. All rights reserved.
              </p>
              <ListGroup horizontal className="socialInfo">
                <ListGroup.Item className="active">
                  <a href="#">
                    <FaTwitter />
                  </a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">
                    <FaLinkedinIn />
                  </a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">
                    <FaRedditAlien />
                  </a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href="#">
                    <FaGithub />
                  </a>
                </ListGroup.Item>
              </ListGroup>
            </div>
            <p className="footerSubText">
              <span className="text-white">DISCLAIMER:</span> The information
              contained herein is for illustration and discussion purpose only
              and does not constitute an offer to sell, or the solicitation of
              an offer to buy any assets, securities or interests in any
              jurisdiction, and shall not constitute an offer, solicitation or
              sale in any jurisdiction in which such offer, solicitation or sale
              would be prohibited, illegal or against the law prior to the
              registration or qualification under the securities laws of that
              jurisdiction. No representation or warranty, express or implied,
              is given by LMEX as to the accuracy, reliability, or completeness
              of the information and opinions contained herein. You are
              recommended to seek independent legal and financial advice before
              taking any initiative in connection with LMEX.
            </p>
          </Footer>
        </Col>
      </Row>
    </Container>
  );
};

export default PageFooter;
