import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/common.css";
import "./assets/fonts/lemux-fonts.css";

import SignupForm from "./Components/SignupForm/SignupForm";
import PrivacyPolicy from "./Components/PrivacyPolicy/PrivacyPolicy";
import Header from "./Components/Header/Header";
import AboutUs from "./Components/AboutUs/AboutUs";
import HelpSupport from "./Components/HelpSupport/HelpSupport";
import Market from "./Components/Market/Market";
import Home from "./Components/Home/Home";

function App() {
  return (
    <>
      <Router>
        <Header />
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/signup" element={<SignupForm />}></Route>
          <Route path="/privacy-policy" element={<PrivacyPolicy />}></Route>
          <Route path="/help-and-support" element={<HelpSupport />}></Route>
          <Route path="/market" element={<Market />}></Route>
          <Route path="/about-us" element={<AboutUs />}></Route>
        </Routes>
      </Router>
    </>
  );
}

export default App;
